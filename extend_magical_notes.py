import magenta.music as mm
from magenta.models.melody_rnn import melody_rnn_sequence_generator
from magenta.protobuf import generator_pb2

input_folder = "input/"
output_folder = "output/"

magical_notes_proto = mm.musicxml_reader.musicxml_file_to_sequence_proto(input_folder + 'MagicalNotes.mxl')

# Download and initialize the model.
print('Downloading model bundle. This will take less than a minute...')
mm.notebook_utils.download_bundle('basic_rnn.mag', 'models/')
bundle = mm.sequence_generator_bundle.read_bundle_file('models/basic_rnn.mag')
print("Initializing Melody RNN...")
generator_map = melody_rnn_sequence_generator.get_generator_map()
melody_rnn = generator_map['basic_rnn'](checkpoint=None, bundle=bundle)
melody_rnn.initialize()
print('Done!')

# Model options. Change these to get different generated sequences!
input_sequence = magical_notes_proto  # change this to another input sequence if you want
num_steps = 2056  # change this for shorter or longer sequences
temperature = 1.0  # the higher the temperature the more random the sequence.

# Set the start time to begin on the next step after the last note ends.
last_end_time = (max(n.end_time for n in input_sequence.notes)
                 if input_sequence.notes else 0)
qpm = input_sequence.tempos[0].qpm
seconds_per_step = 60.0 / qpm / melody_rnn.steps_per_quarter
total_seconds = num_steps * seconds_per_step

generator_options = generator_pb2.GeneratorOptions()
generator_options.args['temperature'].float_value = temperature
generate_section = generator_options.generate_sections.add(
    start_time=last_end_time + seconds_per_step,
    end_time=total_seconds)

# Ask the model to continue the sequence.
sequence = melody_rnn.generate(input_sequence, generator_options)

# Write output to MIDI file
mm.sequence_proto_to_midi_file(sequence, output_folder + 'sequence.mid')

# The following 2 functions only work on Jupyter notebooks
mm.plot_sequence(sequence)
mm.play_sequence(sequence, synth=mm.fluidsynth)
